#!/bin/sh
# Look for user name in sudoers. Add if doesn't exist. username must be the first argument after the scipt. 
# This is a workaround for when credentials get auto removed. 
# Checks to see if file exists as a flag to run. Set the FILE var.
# crontab example: * 2 * * * /bin/bash /path/to/file/sudoers_update.sh User1
FILE='/path/to/.dosudo'
if [ -f $FILE ]; then
    if ! grep -qF -- "$1" "/etc/sudoers" ; then
        echo "$1    ALL=(ALL:ALL) ALL" >> /etc/sudoers;
    fi
    if groups $1 | grep -q -w admin; 
    then 
        exit;
    else 
        dscl . -append /groups/admin GroupMembership $1; 
    fi
fi