#!/bin/bash
# Backup dir to selected source
# Takes three arguments, archive file name, input directory, and output directory. Will output as tar.
# Example: $ backup_script.sh /path/to/target /path/to/dest

# What to backup.
backup_files="$2"
# Where to backup to.
dest="$3"

# Create archive filename.
day=$(date +%A)
filename="$1"
archive_file="$filename-$day.tgz"

# Print start status message.
echo "Backing up $backup_files to $dest/$archive_file"
date

# Backup the files using tar.
tar czf $dest/$archive_file $backup_files

# Print end status message.
echo "Backup finished"
date
